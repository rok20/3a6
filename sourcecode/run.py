#!/usr/bin/env python3

########################################
# 3A6 Heat Transfer Extension Activity #
########################################

# import os,sys
print('Please wait...')
import time
import datetime
import board,busio
import RPi.GPIO as GPIO
import numpy as np
import adafruit_mlx90640
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button
import os
import psutil

# output csv file
def filename():
    filename = "/home/lab_3a6/3A6/run/data/data-"+datetime.datetime.now().strftime("%Y%m%d-%H%M%S")+".csv"
    return filename

global data,fan,flag,fanDuty

# acquisition time
tmax = 600 # seconds (edit to allow longer)
flag = True # do not quit

# %%%% thermal camera %%%%%%%%%%%%%%%%%%%%%
i2c = busio.I2C(board.SCL, board.SDA, frequency=2000000) # setup I2C
mlx = adafruit_mlx90640.MLX90640(i2c) # begin MLX90640 with I2C comm
mlx.refresh_rate = adafruit_mlx90640.RefreshRate.REFRESH_2_HZ # set refresh rate
mlx_shape = (24,32)

# %%%% fan parameters %%%%%%%%%%%%%%%%%%%%%
fanPin = 14 # GPIO pin ID (not to be changed)
fanHz = 100 # fan speed in Hz
fanDuty = 0 # fan duty cycle (values from 0 to 100)
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM) # set GPIO to map GPIO port number
GPIO.setup(fanPin,GPIO.OUT) # set pin to output signal
fan = GPIO.PWM(fanPin, fanHz)
fan.start(fanDuty)

# %%%% fan setup (not to be changed) %%%%%%%%%%%%%%%%%%%%%
def fanSetup(fan_speed):
    global fan,fanDuty
    fanDuty = fan_speed
    fan.ChangeDutyCycle(fanDuty)
    return()

# %%%% fan setup %%%%%%%%%%%%%%%%%%%%%

# %%%% CPU temperature %%%%%%%%%%%%%%%%%
def CPUTemperature():
    with open("/sys/class/thermal/thermal_zone0/temp", "r") as f:
        CPUTemp = int(f.read())/1000.
    return CPUTemp

# %%%% csv file for saving data (replace name so as not to overwrite) %%%%%%%%%%%%%%%%%
def writefiles(data):
    csvname = filename()
    dataout = np.asarray(data)
    np.savetxt(csvname,dataout,delimiter=',',fmt='%.6s')
    print("File saved as '%s.'" % csvname)
    data.clear()
    return()

def save(event):
    writefiles(data)
    return()

# %%%% quit module for button %%%%%%%%%%%%%%%%%
def stop(event):
    global flag,fan
    if flag:
        writefiles(data)
        flag = False
        print('Quit in 5 seconds...')
        time.sleep(5)
    fan.stop()
    return()
    
# %%%% slider speed change module %%%%%%%%%%%%%%%%%
def sliderFanSpeed(fanDuty):
    fanSetup(fanDuty)
    return()
    

# %%%% setup the figure for plotting %%%%%%%%%%%%%%%%%
plt.ion() # enables interactive plotting so it refreshes automatically
fig,ax = plt.subplots(figsize=(24/4,32/4))
temp = ax.imshow(np.zeros(mlx_shape),interpolation='none') #start plot with zeros
temp.set_clim(vmin=30,vmax=55) # set fixed bounds
#temp.set_clim(vmin=10,vmax=60) # set bounds
cbar = fig.colorbar(temp) # setup colorbar for temps
cbar.set_label('Temperature [$^{\circ}$C]',fontsize=14) # colorbar label

frame = np.zeros((24*32,)) # setup array for storing all 768 temperatures
data = [] # data array for storing all frames
t1 = time.monotonic()
tt = 0

# %%%%% plotting ui %%%%%%%%%%%%%%%%
# adding quit button
axquit = plt.axes([0.6, 0.025, 0.1, 0.075])
bquit = Button(axquit, 'Quit')
bquit.on_clicked(stop)

# adding save button
axsave = plt.axes([0.5, 0.025, 0.1, 0.075])
bsave = Button(axsave, 'Save')
bsave.on_clicked(save)

# adding fan speed slider
axfandc = fig.add_axes([0.2, 0.125, 0.5, 0.03])
dutycycle_slider = Slider(
    ax=axfandc,
    label='Fan (%)',
    valmin=0,
    valmax=100,
    valinit=fanDuty,
    valstep=10, # speed increment in 5%
)
dutycycle_slider.on_changed(sliderFanSpeed)

# %%%% setup the figure for plotting %%%%%%%%%%%%%%%%%
try:
    while tt<tmax and flag==True:
        tt = time.monotonic()-t1
        mlx.getFrame(frame) # read MLX temperatures into frame var
        data_array = (np.reshape(frame,mlx_shape)) # reshape to 24x32
        temp.set_data(np.fliplr(data_array)) # flip left to right
        cpu_temp=round(CPUTemperature(),1)
        cpu_util=psutil.cpu_percent()
        temp.axes.text(0.1,-0.3,'time='+f"{tt:.3f}",transform=ax.transAxes,backgroundcolor=[1,1,1])
        #   background color [1 1 1] is necessary so that it overwrites the other number
        plt.pause(1/8) # required
        plt.show()

        datasnap = [tt,cpu_temp]
        #zsave = data_array.reshape(24*32,1).tolist()
        datasnap.extend(frame)
        datasnap.extend([cpu_util,fanDuty])
        data.append(datasnap)

        #print('Sample Rate: {0:2.1f}fps'.format(len(t_array)/np.sum(t_array)))
        print('Time: {0:2.1f}s, CPU temp (C): {1:2.1f}, utilisation: {2:2.1f}%'.format(tt,cpu_temp,cpu_util))
    
except KeyboardInterrupt: # ~trap a CTRL+C keyboard interrupt
    stop(None) #save and quit

# writefiles(data)
