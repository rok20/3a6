# Part IIA Experiment - 3A6 - Heat Transfer over CPU in a Raspberry Pi

This Git repository provides you with all basic codes aimed to assist you in completing 3A6 coursework.

## Download the code
You can download the source code from this repository. If you are not too familiar with the path and command line, we strongly recommend you work within the downloaded directory. You can enter the working directory in the command line using
```bash
cd path/
```
where ``path/`` is the directory containing the ``.ipynb`` file.

## Copying data files
The data files saved to your USB during the lab session should be copy to the ``data`` folder in your working directory.

## Installation

You can run the Notebook on your own computer following the instructions below or work online by uploading it to [Google Colab](https://colab.research.google.com/). 

### Initialising virtual environment

We strongly encourage you to use [Python 3](https://www.python.org/downloads/) and create a virtual environment at ``path/to/venv`` (you can choose your own path). This ensures the modules used by the other projects do not interfere.
```bash
python3 -m venv path/to/venv
```
if calling ``python3`` reports an error, make sure you installed Python 3 (sometimes it can be called by ``python``, but note that it should not be confused with Python 2)

### Activating the virtual environment
You must activate the virtual environment before it can work. Remember to replace ``path/to/venv``

Windows:
```powershell
path/to/venv\Scripts\activate
```

Linux/macOS
```bash
source path/to/venv/bin/activate
```

### Install required packages

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install numpy.

```bash
pip install numpy matplotlib pandas notebook
```

## Usage
Start Jupyter Notebook by using the command and setting the path to our working directory ``path/``
```bash
jupyter notebook --notebook-dir 'path/'
```

Jupyter Notebook should automatically open in your browser and you should be able to navigate from here.

### For lab sessions after 27 Feb
The ourput data includes CPU utilisation and fan speed. Your are exepcted to use all data in your exported datafile, so please uncomment (removing ``#`` at the start of line) the code to process these data in the notebook.
```python3
v2 = True
cpu_util = df [770]
fan_cycl = df [771]
```

## Contributing
We welcome any bug report and contribution to the code is also encouraged. Please email [Schoen Cho](mailto:zc282@cam.ac.uk) or [Prof. Simone Hochgreb](mailto:sh372@cam.ac.uk) if there is any problem.
